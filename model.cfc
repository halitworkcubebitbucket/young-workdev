<cfcomponent>

    <cffunction name="create">
        <cfargument name="malzemeadi" type="string">
        <cfargument name="birimfiyat" type="numeric">
        <cfargument name="birimgalori" type="numeric">
        <cfargument name="miktar" type="numeric">

        <cfquery name="create_query" datasource="tarifler">
            INSERT INTO [malzemeler]
           ([malzemeadi]
           ,[birimfiyat]
           ,[birimgalori]
           ,[miktar])
            VALUES
           (<cfqueryparam cfsqltype="CF_SQL_NVARCHAR" value="#arguments.malzemeadi#">
           ,<cfqueryparam cfsqltype="CF_SQL_DECIMAL" scale="2" value="#arguments.birimfiyat#">
           ,<cfqueryparam cfsqltype="CF_SQL_DECIMAL" scale="2" value="#arguments.birimgalori#">
           ,<cfqueryparam cfsqltype="CF_SQL_DECIMAL" scale="2" value="#arguments.miktar#">)
        </cfquery>

    </cffunction>

    <cffunction name="read">
        <cfargument name="malzemelerid" type="numeric" default="0">
        <cfquery name="read_query" datasource="tarifler">
        SELECT * FROM [malzemeler]
        <cfif arguments.malzemelerid neq 0>
        WHERE malzemelerid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.malzemelerid#">
        </cfif>
        </cfquery>
        <cfreturn read_query>
    </cffunction>

    <cffunction name="update">
        <cfargument name="malzemeadi" type="string">
        <cfargument name="birimfiyat" type="numeric">
        <cfargument name="birimgalori" type="numeric">
        <cfargument name="miktar" type="numeric">
        <cfargument name="malzemelerid" type="numeric">
        <cfquery name="read_query" datasource="tarifler">
        UPDATE [malzemeler]
        SET [malzemeadi] = <cfqueryparam cfsqltype="CF_SQL_NVARCHAR" value="#arguments.malzemeadi#">
            ,[birimfiyat] = <cfqueryparam cfsqltype="CF_SQL_DECIMAL" scale="2" value="#arguments.birimfiyat#">
            ,[birimgalori] = <cfqueryparam cfsqltype="CF_SQL_DECIMAL" scale="2" value="#arguments.birimgalori#">
            ,[miktar] = <cfqueryparam cfsqltype="CF_SQL_DECIMAL" scale="2" value="#arguments.miktar#">
        WHERE malzemelerid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.malzemelerid#">
        </cfquery>
    </cffunction>

    <cffunction name="delete">
        <cfargument name="malzemelerid" type="numeric">
        <cfquery name="read_query" datasource="tarifler">
        DELETE FROM [malzemeler] 
        WHERE malzemelerid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.malzemelerid#">
        </cfquery>
    </cffunction>

</cfcomponent>