<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <cfobject name="mymodel" type="component" component="model">
    <cfset list = mymodel.read()>
    <cfset totalprice = 0>
    <cfset totalcalori= 0>
    <table border="1" style="width: 100%">
        <thead>
            <tr>
                <td></td>
                <th>Malzeme Adı</th>
                <th>Birim Fiyat</th>
                <th>Birim Kalori</th>
                <th>Miktar</th>
                <th>Toplam Fiyat</th>
                <th>Toplam Kalori</th>
            </tr>
        </thead>
        <tbody>
            <cfoutput query="list">
            <tr>
                <td> <a href="update.cfm?malzemelerid=#list.malzemelerid#">Güncelle</a> <a href="delete.model.cfm?malzemelerid=#list.malzemelerid#" onclick="return confirm('Silecen mi')">Sil</a> </td>
                <td>#list.malzemeadi#</td>
                <td>#list.birimfiyat#</td>
                <td>#list.birimgalori#</td>
                <td>#list.miktar#</td>
                <td>#( list.birimfiyat * list.miktar )#</td>
                <td>#( list.birimgalori * list.miktar )#</td>
            </tr>
            <cfset totalprice = totalprice + ( list.birimfiyat * list.miktar )>
            <cfset totalcalori = totalcalori + ( list.birimgalori * list.miktar )>
            </cfoutput>
        </tbody>
        <tfoot>
            <cfoutput>
            <tr>
                <td></td>

                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>#totalprice#</td>
                <td>#totalcalori#</td>

            </tr>
        </cfoutput>
        </tfoot>
    </table>
    <a href="add.cfm">Ekle</a>
	<a href="list.cfm">Yenile</a>
</body>
</html>